#! /usr/bin/env python
#_*_ encoding: utf-8 _*_

import ovirtsdk4 as sdk
import ovirtsdk4.types as types
import datetime
import os
from time import strftime

#Conexión a la api
class api(object):
    def __init__(self, **kwargs):
        self.connection = sdk.Connection(
            url='https://' + kwargs['url'],
            username=kwargs['username'] + '@' + kwargs['domain'],
            password=kwargs['password'],
            ca_file=kwargs['ca_file'],
            debug=True
        )

    def connect(self):
        return self.connection

    def disconnect(self):
        self.connection.close()

#Clase de eventos
class events(object):
    def __init__(self, conexion):
        self.events = conexion.system_service().events_service().list()

#Lista de todos los eventos
    def events_list(self):
        return self.events

#Seleccion del ultimo evento de cada MV, el break es debido a que el ultimo eventos es el primero que entra en el bucle cumpliendo la condicion
#Nos ahorramos un while.
    def last_event(self, name):
        for event in self.events:
            if name in event.description and 'got disconnected' in event.description:
                self.max_date=event.time
                break
            else:
                self.max_date=None
        if self.max_date != None:
                self.result =  name+" "+self.max_date.strftime("%Y-%m-%d %H:%M:%S")
        else:
                self.result = name
        return self.result

#Metodo logger para reflejar en el log lo ocurrido
    def logger(self, list_to_shutdown):
        if not os.path.exists("/opt/rhev_vmpoweroff/log"):
           os.makedirs("/opt/rhev_vmpoweroff/log")

        self.dt = datetime.datetime.now()
        self.current_year = self.dt.strftime("%Y")
        self.current_month = self.dt.strftime("%B")
        self.current_day = self.dt.strftime("%d")

        with open('/opt/rhev_vmpoweroff/log/' + self.current_year + '_' + self.current_month + '.log', 'a+') as self.logfile:
            self.logfile.write("<------------------------------------------------------------------>" + "\n")
            self.logfile.write("VM to poweroff in " + self.current_day + " " + self.current_month + "\n")
            for vm_to_shutdown in list_to_shutdown:
                self.logfile.write(vm_to_shutdown + "\n")
            self.logfile.write("<------------------------------------------------------------------>" + "\n")
            self.logfile.close()

#Clase compute donde se encuentran todos los metodos relacionados con MV
class compute(object):
    def __init__(self, conexion):
        self.vms_conn = conexion.system_service().vms_service().list()
        self.mv_conn = conexion

    def vm_list(self):
        return self.vms_conn

    def stop_vm(self, vm_date):
        self.dt = datetime.datetime.now() - datetime.timedelta(days=1)
        self.date = self.dt.strftime("%Y-%m-%d")
        self.date = vm_date.split(' ')
        if len(self.date) > 1:
           self.date_last_conn = self.date[1]
           self.vm_name = self.date[0]
           if self.date_last_conn <= self.dt.strftime("%Y-%m-%d"):
               self.vms_service = self.mv_conn.system_service().vms_service()
               self.vm = self.vms_service.list(search='name={}'.format(self.vm_name))[0]
               if self.vm.status != 'down':
                  self.vm_service = self.vms_service.vm_service(self.vm.id)
                  #vm_service.stop
                  return self.vm_name