#! /usr/bin/env python
#_*_ encoding: utf-8 _*_

import ovirtsdk4 as sdk
import ovirtsdk4.types as types

from rhev_vmpoweroff_classes import api, compute, events
from simplejson import load as json_load

with open('settings.json', 'r') as settings_file:
     settings = json_load(settings_file)

#API connect
conn = api(url='ovirt-lab-engine.gmvtest.es/ovirt-engine/api',
           username=settings['username'],
           password=settings['password'],
           domain=settings['domain'],
           ca_file='ca.pem',
          )

engine = conn.connect()

compute_api = compute(engine)
events_api = events(engine)

#Lista de maquinas virtuales
vms = compute_api.vm_list()
list_to_shutdown = []

#Process to shutdown VM
for vm in vms:
  if vm.name != 'HostedEngine':
     vm_date = events_api.last_event(vm.name)
     vm_to_shutdown = compute_api.stop_vm(vm_date)
     if vm_to_shutdown != None:
         list_to_shutdown.append(vm_to_shutdown)

#Log output
events_api.logger(list_to_shutdown)

conn.disconnect()